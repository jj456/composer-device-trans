<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/8/6 0006
 * Time: 21:57
 */

namespace DeviceTrans;


class DeviceTransService
{
    //applist分析
    //size_parms = ['4.66'=>'less','7.22'=>'more']
    public function analyzeAppList($apply_time, $app_list, $loan_app_list, $size_parms)
    {
        //处理applist 数据
        array_walk($app_list, function (&$item) {
            if (!isset($item['appSize'])) {
                $item['appSize'] = 0;
            } else {
                $appSize = str_replace(' ', '', $item['appSize']);//去除字符串空格
                $appSize = strtoupper($appSize);//将字符串转大写
                if (strpos($appSize, ',') !== false) {//将逗号转.
                    $appSize = str_replace(',', '.',$appSize);
                }
                if (strpos($appSize, 'MB') !== false) {
                    $item['appSize'] = str_replace('MB', '',$appSize);
                }elseif (strpos($appSize, 'KB') !== false) {
                    $appSize = str_replace('KB', '',$appSize);
                    $item['appSize'] =is_numeric($appSize)?$appSize/1024:0;
                }else {
                    $item['appSize'] = 0;
                }
            }
            if (!isset($item['app_name'])) $item['app_name'] = "";
            if (!isset($item['package_name'])) $item['package_name'] = "";
            if (!isset($item['version_name'])) $item['version_name'] = "";
            if (!isset($item['versionCode'])) $item['versionCode'] = "";
            if (!isset($item['first_install_time'])) $item['first_install_time'] = "";
            if (!isset($item['last_update_time'])) $item['last_update_time'] = "";
            if (!isset($item['appDir'])) $item['appDir'] = "";
            if (!isset($item['appType'])) $item['appType'] = "";
        });
        $collect_applist = collect($app_list);
        $before_1_day_time = $this->getBeforeTimeWithCurrentT(1, $apply_time);
        $before_3_day_time = $this->getBeforeTimeWithCurrentT(3, $apply_time);
        $before_5_day_time = $this->getBeforeTimeWithCurrentT(5, $apply_time);
        $before_7_day_time = $this->getBeforeTimeWithCurrentT(7, $apply_time);
        $before_14_day_time = $this->getBeforeTimeWithCurrentT(14, $apply_time);
        $before_30_day_time = $this->getBeforeTimeWithCurrentT(30, $apply_time);
        $before_60_day_time = $this->getBeforeTimeWithCurrentT(60, $apply_time);
        //安装app分析
        $alst_install_app_num_1d = $collect_applist->where('first_install_time', '>=', $before_1_day_time)->where('first_install_time', '<=', $apply_time)->count();//一天内安装app数量
        $alst_install_app_num_3d = $collect_applist->where('first_install_time', '>=', $before_3_day_time)->where('first_install_time', '<=', $apply_time)->count();//3天内安装app数量
        $alst_install_app_num_5d = $collect_applist->where('first_install_time', '>=', $before_5_day_time)->where('first_install_time', '<=', $apply_time)->count();//5天内安装app数量
        $alst_install_app_num_7d = $collect_applist->where('first_install_time', '>=', $before_7_day_time)->where('first_install_time', '<=', $apply_time)->count();//7天内安装app数量
        $alst_install_app_num_14d = $collect_applist->where('first_install_time', '>=', $before_14_day_time)->where('first_install_time', '<=', $apply_time)->count();//14天内安装app数量
        $alst_install_app_num_30d = $collect_applist->where('first_install_time', '>=', $before_30_day_time)->where('first_install_time', '<=', $apply_time)->count();//30天内安装app数量
        $alst_install_app_num_60d = $collect_applist->where('first_install_time', '>=', $before_60_day_time)->where('first_install_time', '<=', $apply_time)->count();//60天内安装app数量
        //借贷类app样本
        //安装借贷类app分析
        $alst_install_loan_app_num_1d = $collect_applist->whereIn("package_name", $loan_app_list)->where('first_install_time', '>=', $before_1_day_time)->where('first_install_time', '<=', $apply_time)->count();
        $alst_install_loan_app_num_3d = $collect_applist->whereIn("package_name", $loan_app_list)->where('first_install_time', '>=', $before_3_day_time)->where('first_install_time', '<=', $apply_time)->count();
        $alst_install_loan_app_num_5d = $collect_applist->whereIn("package_name", $loan_app_list)->where('first_install_time', '>=', $before_5_day_time)->where('first_install_time', '<=', $apply_time)->count();
        $alst_install_loan_app_num_7d = $collect_applist->whereIn("package_name", $loan_app_list)->where('first_install_time', '>=', $before_7_day_time)->where('first_install_time', '<=', $apply_time)->count();
        $alst_install_loan_app_num_14d = $collect_applist->whereIn("package_name", $loan_app_list)->where('first_install_time', '>=', $before_14_day_time)->where('first_install_time', '<=', $apply_time)->count();
        $alst_install_loan_app_num_30d = $collect_applist->whereIn("package_name", $loan_app_list)->where('first_install_time', '>=', $before_30_day_time)->where('first_install_time', '<=', $apply_time)->count();
        $alst_install_loan_app_num_60d = $collect_applist->whereIn("package_name", $loan_app_list)->where('first_install_time', '>=', $before_60_day_time)->where('first_install_time', '<=', $apply_time)->count();
        //app大小分析

        $res_size = [];
        foreach ($size_parms as $key => $parm) {
            $str_name = 'alst_app_size_' . $parm . '_num_' . str_replace('.', '', $key);
            $operator = $parm == "less" ? '<=' : '>=';
            $res_size[$str_name] = $collect_applist->where('appSize', $operator, $key)->count();
        }

        //安装时间和更新时间
        $alst_first_last_equal_num = 0;//安装时间和更新时间相同
        $alst_first_last_more_30_num = 0;//安装时间和更新时间间隔30天
        $alst_first_last_more_60_num = 0;//安装时间和更新时间间隔60天
        $alst_first_last_more_90_num = 0;//安装时间和更新时间间隔90天
        foreach ($collect_applist as $value) {
            $first_strt = strtotime($value['first_install_time']);
            $last_strt = strtotime($value['last_update_time']);
            $difday = $this->diff_days(date('Y-m-d', $first_strt), date('Y-m-d', $last_strt));
            if ($difday == 0) $alst_first_last_equal_num++;
            if ($difday >= 30) $alst_first_last_more_30_num++;
            if ($difday >= 60) $alst_first_last_more_60_num++;
            if ($difday >= 90) $alst_first_last_more_90_num++;
        }
        //更新app分析
        $alst_update_app_num_1d = $collect_applist->where('last_update_time', '>=', $before_1_day_time)->where('first_install_time', '<=', $apply_time)->count();//一天内更新app数量
        $alst_update_app_num_3d = $collect_applist->where('last_update_time', '>=', $before_3_day_time)->where('first_install_time', '<=', $apply_time)->count();//3天内更新app数量
        $alst_update_app_num_5d = $collect_applist->where('last_update_time', '>=', $before_5_day_time)->where('first_install_time', '<=', $apply_time)->count();//5天内更新app数量
        $alst_update_app_num_7d = $collect_applist->where('last_update_time', '>=', $before_7_day_time)->where('first_install_time', '<=', $apply_time)->count();//7天内更新app数量
        $alst_update_app_num_14d = $collect_applist->where('last_update_time', '>=', $before_14_day_time)->where('first_install_time', '<=', $apply_time)->count();//14天内更新app数量
        $alst_update_app_num_30d = $collect_applist->where('last_update_time', '>=', $before_30_day_time)->where('first_install_time', '<=', $apply_time)->count();//30天内更新app数量
        $alst_update_app_num_60d = $collect_applist->where('last_update_time', '>=', $before_60_day_time)->where('first_install_time', '<=', $apply_time)->count();//60天内更新app数量
        //更新借贷类app分析
        $alst_update_loan_app_num_1d = $collect_applist->whereIn("package_name", $loan_app_list)->where('last_update_time', '>=', $before_1_day_time)->where('first_install_time', '<=', $apply_time)->count();//一天内更新借贷类app数量
        $alst_update_loan_app_num_3d = $collect_applist->whereIn("package_name", $loan_app_list)->where('last_update_time', '>=', $before_3_day_time)->where('first_install_time', '<=', $apply_time)->count();//3天内更新借贷类app数量
        $alst_update_loan_app_num_5d = $collect_applist->whereIn("package_name", $loan_app_list)->where('last_update_time', '>=', $before_5_day_time)->where('first_install_time', '<=', $apply_time)->count();//5天内更新借贷类app数量
        $alst_update_loan_app_num_7d = $collect_applist->whereIn("package_name", $loan_app_list)->where('last_update_time', '>=', $before_7_day_time)->where('first_install_time', '<=', $apply_time)->count();//7天内更新借贷类app数量
        $alst_update_loan_app_num_14d = $collect_applist->whereIn("package_name", $loan_app_list)->where('last_update_time', '>=', $before_14_day_time)->where('first_install_time', '<=', $apply_time)->count();//14天内更新借贷类app数量
        $alst_update_loan_app_num_30d = $collect_applist->whereIn("package_name", $loan_app_list)->where('last_update_time', '>=', $before_30_day_time)->where('first_install_time', '<=', $apply_time)->count();//30天内更新借贷类app数量
        $alst_update_loan_app_num_60d = $collect_applist->whereIn("package_name", $loan_app_list)->where('last_update_time', '>=', $before_60_day_time)->where('first_install_time', '<=', $apply_time)->count();//60天内更新借贷类app数量
        $result = [
            'alst_install_app_num_1d' => $alst_install_app_num_1d,
            'alst_install_app_num_3d' => $alst_install_app_num_3d,
            'alst_install_app_num_5d' => $alst_install_app_num_5d,
            'alst_install_app_num_7d' => $alst_install_app_num_7d,
            'alst_install_app_num_14d' => $alst_install_app_num_14d,
            'alst_install_app_num_30d' => $alst_install_app_num_30d,
            'alst_install_app_num_60d' => $alst_install_app_num_60d,
            'alst_install_loan_app_num_1d' => $alst_install_loan_app_num_1d,
            'alst_install_loan_app_num_3d' => $alst_install_loan_app_num_3d,
            'alst_install_loan_app_num_5d' => $alst_install_loan_app_num_5d,
            'alst_install_loan_app_num_7d' => $alst_install_loan_app_num_7d,
            'alst_install_loan_app_num_14d' => $alst_install_loan_app_num_14d,
            'alst_install_loan_app_num_30d' => $alst_install_loan_app_num_30d,
            'alst_install_loan_app_num_60d' => $alst_install_loan_app_num_60d,
            'alst_update_app_num_1d' => $alst_update_app_num_1d,
            'alst_update_app_num_3d' => $alst_update_app_num_3d,
            'alst_update_app_num_5d' => $alst_update_app_num_5d,
            'alst_update_app_num_7d' => $alst_update_app_num_7d,
            'alst_update_app_num_14d' => $alst_update_app_num_14d,
            'alst_update_app_num_30d' => $alst_update_app_num_30d,
            'alst_update_app_num_60d' => $alst_update_app_num_60d,
            'alst_update_loan_app_num_1d' => $alst_update_loan_app_num_1d,
            'alst_update_loan_app_num_3d' => $alst_update_loan_app_num_3d,
            'alst_update_loan_app_num_5d' => $alst_update_loan_app_num_5d,
            'alst_update_loan_app_num_7d' => $alst_update_loan_app_num_7d,
            'alst_update_loan_app_num_14d' => $alst_update_loan_app_num_14d,
            'alst_update_loan_app_num_30d' => $alst_update_loan_app_num_30d,
            'alst_update_loan_app_num_60d' => $alst_update_loan_app_num_60d,
            'alst_first_last_equal_num' => $alst_first_last_equal_num,
            'alst_first_last_more_30_num' => $alst_first_last_more_30_num,
            'alst_first_last_more_60_num' => $alst_first_last_more_60_num,
            'alst_first_last_more_90_num' => $alst_first_last_more_90_num,
        ];
        $return_res = array_merge($result, $res_size);
        return $return_res;
    }

    //deviceinfo 分析
    public function analyzeDeviceInfo($device_info)
    {
        $dev_brand = isset($device_info['hardware']['brand']) ? strtolower($device_info['hardware']['brand']) : -99;
        $dev_ram_total_size = isset($device_info['storage']['ram_total_size']) ? (int)$device_info['storage']['ram_total_size'] : -99;
        $dev_ram_used_size = isset($device_info['storage']['ram_used_size']) ? (int)$device_info['storage']['ram_used_size'] : -99;
        $dev_memory_total_size = isset($device_info['storage']['memory_total_size']) ? (int)$device_info['storage']['memory_total_size'] : -99;
        $dev_memory_used_size = isset($device_info['storage']['memory_used_size']) ? (int)$device_info['storage']['memory_used_size'] : -99;
        $dev_memory_card_total_size = isset($device_info['storage']['memory_card_total_size']) ? (int)$device_info['storage']['memory_card_total_size'] : -99;
        $dev_memory_card_used_size = isset($device_info['storage']['memory_card_used_size']) ? (int)$device_info['storage']['memory_card_used_size'] : -99;
        $dev_and_id = isset($device_info['general_data']['and_id']) ? $device_info['general_data']['and_id'] : -99;
        $dev_network_type = isset($device_info['general_data']['network_type']) ? $device_info['general_data']['network_type'] : -99;
        $dev_simulator = isset($device_info['other_data']['simulator']) && $device_info['other_data']['simulator'] ? 1 : 0;
        $dev_wifi_count = isset($device_info['network']['wifi_count']) ? $device_info['network']['wifi_count'] : -99;
        $dev_battery_pct = isset($device_info['battery_status']['battery_pct']) ? $device_info['battery_status']['battery_pct'] : -99;
        $dev_is_charging = isset($device_info['battery_status']['is_charging']) && $device_info['battery_status']['is_charging'] ? 1 : 0;
        $dev_is_usb_charge = isset($device_info['battery_status']['is_usb_charge']) && $device_info['battery_status']['is_usb_charge'] ? 1 : 0;
        $dev_is_ac_charge = isset($device_info['battery_status']['is_ac_charge']) && $device_info['battery_status']['is_ac_charge'] ? 1 : 0;
        $dev_audio_external = isset($device_info['file_cnt']['audio_external']) ? $device_info['file_cnt']['audio_external'] : -99;
        $dev_images_external = isset($device_info['file_cnt']['images_external']) ? $device_info['file_cnt']['images_external'] : -99;
        $dev_video_external = isset($device_info['file_cnt']['video_external']) ? $device_info['file_cnt']['video_external'] : -99;
        $dev_audio_internal = isset($device_info['file_cnt']['audio_internal']) ? $device_info['file_cnt']['audio_internal'] : -99;
        $dev_images_internal = isset($device_info['file_cnt']['images_internal']) ? $device_info['file_cnt']['images_internal'] : -99;
        $dev_video_internal = isset($device_info['file_cnt']['video_internal']) ? $device_info['file_cnt']['video_internal'] : -99;
        //判断设备品牌是否含有infinix 包含有字符串infinix，则dev_brand_infinix取值为1，否则为0.
        if (strpos($dev_brand, 'infinix') !== false) {
            $dev_brand_infinix = 1;
        }else{
            $dev_brand_infinix = 0;
        }

        $result = [
            'dev_brand' => $dev_brand,
            'dev_brand_infinix' => $dev_brand_infinix,
            'dev_ram_total_size' => $dev_ram_total_size,
            'dev_ram_used_size' => $dev_ram_used_size,
            'dev_memory_total_size' => $dev_memory_total_size,
            'dev_memory_used_size' => $dev_memory_used_size,
            'dev_memory_card_total_size' => $dev_memory_card_total_size,
            'dev_memory_card_used_size' => $dev_memory_card_used_size,
            'dev_and_id' => $dev_and_id,
            'dev_network_type' => $dev_network_type,
            'dev_simulator' => $dev_simulator,
            'dev_wifi_count' => $dev_wifi_count,
            'dev_battery_pct' => $dev_battery_pct,
            'dev_is_charging' => $dev_is_charging,
            'dev_is_usb_charge' => $dev_is_usb_charge,
            'dev_is_ac_charge' => $dev_is_ac_charge,
            'dev_audio_external' => $dev_audio_external,
            'dev_images_external' => $dev_images_external,
            'dev_video_external' => $dev_video_external,
            'dev_audio_internal' => $dev_audio_internal,
            'dev_images_internal' => $dev_images_internal,
            'dev_video_internal' => $dev_video_internal,
        ];
        return $result;

    }

    //获取app列表信息+设备信息之和
    public function getAllAppListAndDeviceInfoData($apply_time, $app_list, $loan_app_list, $size_parms,$device_info){
        $applist_info = $this->analyzeAppList($apply_time, $app_list, $loan_app_list, $size_parms);
        $device_info = $this->analyzeDeviceInfo($device_info);
        $return_res = array_merge($applist_info, $device_info);
        return $return_res;
    }


    //获取指定时间前几天开始时间
    private function getBeforeTimeWithCurrentT($day, $time)
    {
        $start_time = date('Y-m-d H:i:s', strtotime("-" . $day . " days", strtotime($time)));
        return $start_time;
    }

    private function diff_days($day1, $day2)
    {
        $second1 = strtotime(date('Y-m-d', strtotime($day1)));
        $second2 = strtotime(date('Y-m-d', strtotime($day2)));
        $diff = abs($second1 - $second2);
        return (int)($diff / 86400);
    }

    //获取app列表信息+设备信息+短信之和
    public function getAllAppListAndDeviceInfoAndSmsData($apply_time, $app_list, $loan_app_list, $size_parms,$device_info,$sms_info,$feature_info){
        $applist_info = $this->analyzeAppList($apply_time, $app_list, $loan_app_list, $size_parms);
        $device_info = $this->analyzeDeviceInfo($device_info);
        $sms_info = $this->analyseSmsInfo($apply_time,$sms_info,$feature_info);
        $return_res = array_merge($applist_info, $device_info);
        $res = array_merge($return_res,$sms_info);
        return $res;
    }


    //短信数据分析
    public function analyseSmsInfo($apply_time,$sms_info,$feature_info){
        if (empty($sms_info)) return [];
        foreach ($sms_info as $key=>&$value){
            $date = isset($value['date'])?date('Y-m-d H:i:s',$value['date']/1000):'';
            $date_sort = strtotime($date);
            $value['date'] = $date_sort;
            $value['date_time'] = date('H:i:s',$date_sort);
            $value['address_number'] = is_numeric($value['address'])?1:2;//判断address是否是全数字符串
        }
        $apply_time = strtotime($apply_time);//申请时间
        $overdue_keywords_array = $feature_info['overdue_keywords'];//逾期关键词
        $competitor_keywords_array = $feature_info['competitor_keywords'];//竞品类关键词
        $high_risk_keywords_array = $feature_info['high_risk_keywords'];//高风险类关键词
        $solvency_keywords_array = $feature_info['solvency_keywords'];//偿付能力类关键词
        $loan_app_keywords_array = $feature_info['loan_app_keywords'];//借贷类app列表
        $before_one_day = $apply_time-(24*60*60);//近1天内
        $before_three_day = $apply_time-(24*60*60*3);//近3天内
        $before_five_day = $apply_time-(24*60*60*5);//近5天内
        $before_seven_day = $apply_time-(24*60*60*7);//近7天内
        $before_ten_day = $apply_time-(24*60*60*10);//近10天内
        $before_fourteen_day = $apply_time-(24*60*60*14);//近14天内
        $before_twenty_day = $apply_time-(24*60*60*20);//近20天内
        $before_twenty_one_day = $apply_time-(24*60*60*21);//近21天内
        $before_thirty_day = $apply_time-(24*60*60*30);//近30天内
        $collect_sms_info = collect($sms_info);//短信数据
        $res_array['all_send_count'] = $collect_sms_info->where('type',2)->count();//发送短信数量
        //近多少天内发送短信数量
        $before_send_day = [
            'one'=>$before_one_day,
            'three'=>$before_three_day,
            'five'=>$before_five_day,
            'seven'=>$before_seven_day,
            'ten'=>$before_ten_day,
            'twenty'=>$before_twenty_day,
            'thirty'=>$before_thirty_day
        ];
        foreach ($before_send_day as $key=>$time){
            $res_array['before_'.$key.'_send_count'] = $collect_sms_info->where('type',2)->where('date','>=',$time)
                ->where('date','<=',$apply_time)
                ->count();
        }
        //近多少天内在某时间段内发送短信的数量
        $before_send_time_day = [
            'seven_0_6'=>['date'=>$before_seven_day,'time'=>['00:00:00','06:00:00']],
            'seven_6_12'=>['date'=>$before_seven_day,'time'=>['06:00:00','12:00:00']],
            'seven_12_18'=>['date'=>$before_seven_day,'time'=>['12:00:00','18:00:00']],
            'seven_18_24'=>['date'=>$before_seven_day,'time'=>['18:00:00','23:59:59']],
            'fourteen_0_6'=>['date'=>$before_fourteen_day,'time'=>['00:00:00','06:00:00']],
            'fourteen_6_12'=>['date'=>$before_fourteen_day,'time'=>['06:00:00','12:00:00']],
            'fourteen_12_18'=>['date'=>$before_fourteen_day,'time'=>['12:00:00','18:00:00']],
            'fourteen_18_24'=>['date'=>$before_fourteen_day,'time'=>['18:00:00','23:59:59']],
            'twenty_one_0_6'=>['date'=>$before_twenty_one_day,'time'=>['00:00:00','06:00:00']],
            'twenty_one_6_12'=>['date'=>$before_twenty_one_day,'time'=>['06:00:00','12:00:00']],
            'twenty_one_12_18'=>['date'=>$before_twenty_one_day,'time'=>['12:00:00','18:00:00']],
            'twenty_one_18_24'=>['date'=>$before_twenty_one_day,'time'=>['18:00:00','23:59:59']],
        ];
        foreach ($before_send_time_day as $key=>$time){
            $res_array['before_'.$key.'_send_count'] = $collect_sms_info->where('type',2)
                ->where('date','>=',$time['date'])->where('date','<=',$apply_time)
                ->where('date_time','>=',$time['time'][0])->where('date_time','<=',$time['time'][1])->count();
        }
        //近多少天内在某时间段内接受短信的数量
        foreach ($before_send_time_day as $key=>$time){
            $res_array['before_'.$key.'_receive_count'] = $collect_sms_info->where('type',1)
                ->where('date','>=',$time['date'])->where('date','<=',$apply_time)
                ->where('date_time','>=',$time['time'][0])
                ->where('date_time','<=',$time['time'][1])->count();
        }
        //收取短信总数量
        $res_array['all_receive_count'] = $collect_sms_info->where('type',1)->count();//收取短信数量
        //近多少天内接受短信特征
        $before_receive_day = [
            'one'=>['date'=>$before_one_day],
            'three'=>['date'=>$before_three_day],
            'five'=>['date'=>$before_five_day],
            'seven'=>['date'=>$before_seven_day],
            'ten'=>['date'=>$before_ten_day],
            'twenty'=>['date'=>$before_twenty_day],
            'thirty'=>['date'=>$before_thirty_day],
        ];
        foreach ($before_receive_day as $key=>$time){
            $time_collection = $collect_sms_info->where('type',1)->where('date','>=',$time['date'])->where('date','<=',$apply_time);
            $res_array['before_'.$key.'_receive_count'] = $time_collection->count();
            $res_array['before_'.$key.'_receive_overdue_keywords_count'] = 0;
            foreach ($overdue_keywords_array as $keywords){
                $filte_data = $time_collection->filter(function ($item) use ($keywords) {
                    return false !== stristr($item['body'], $keywords);
                });
                $res_array['before_'.$key.'_receive_overdue_keywords_count'] += count($filte_data);
            }//逾期类关键词的短信数量

            $res_array['before_'.$key.'_receive_competitor_keywords_count'] = 0;
            foreach ($competitor_keywords_array as $keywords){
                $filte_data = $time_collection->filter(function ($item) use ($keywords) {
                    return false !== stristr($item['body'], $keywords);
                });
                $res_array['before_'.$key.'_receive_competitor_keywords_count'] += count($filte_data);
            }//竞品类关键词的短信数量

            $res_array['before_'.$key.'_receive_high_risk_keywords_count'] = 0;
            foreach ($high_risk_keywords_array as $keywords){
                $filte_data = $time_collection->filter(function ($item) use ($keywords) {
                    return false !== stristr($item['body'], $keywords);
                });
                $res_array['before_'.$key.'_receive_high_risk_keywords_count'] +=  count($filte_data);
            }//高风险类关键词的短信数量

            $res_array['before_'.$key.'_receive_solvency_keywords_count'] = 0;
            foreach ($solvency_keywords_array as $keywords){
                $filte_data = $time_collection->filter(function ($item) use ($keywords) {
                    return false !== stristr($item['body'], $keywords);
                });
                $res_array['before_'.$key.'_receive_solvency_keywords_count'] += count($filte_data);
            }//有偿付能力类关键词的短信数量

            $res_array['before_'.$key.'_receive_loan_app_keywords_count'] = 0;
            foreach ($loan_app_keywords_array as $keywords){
                $filte_data = $time_collection->filter(function ($item) use ($keywords) {
                    return false !== stristr($item['body'], $keywords);
                });
                $res_array['before_'.$key.'_receive_loan_app_keywords_count'] += count($filte_data);
            }//有借贷app列表类关键字的短信数量

            $res_array['before_'.$key.'_receive_send_id_in_loan_applist_count'] = 0;
            foreach ($loan_app_keywords_array as $keywords){
                $filte_data = $time_collection->filter(function ($item) use ($keywords) {
                    return false !== stristr($item['address'], $keywords);
                });
                $res_array['before_'.$key.'_receive_send_id_in_loan_applist_count'] += count($filte_data);
            }//send_id在借贷app列表的短信数量
            $branch_ng = 'Branch NG';
            $branch_ng_data = $time_collection->filter(function ($item) use ($branch_ng) {
                return false !== stristr($item['address'], $branch_ng);
            });
            $res_array['before_'.$key.'_receive_send_id_branch_ng_count']=count($branch_ng_data);//send_id为Branch NG的短信数量
            $uba = 'UBA';
            $uba_data = $time_collection->filter(function ($item) use ($uba) {
                return false !== stristr($item['address'], $uba);
            });
            $res_array['before_'.$key.'_receive_send_id_uba_count']=count($uba_data);//send_id为UBA的短信数量
            $res_array['before_'.$key.'_receive_send_id_number_count']=$time_collection->where('address_number',1)->count();//send_id为数字组成的电话号码的短信数量
        }
        return $res_array;
    }
}